﻿using ParqueDiversiones.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueDiversiones
{
    class Program
    {
        public static double costoMenores = 500;
        public static double costoMayores = 1000;
        public static double porcentajeImpuesto = 13;
        public static double tarifaImpuestoExtranjeros = 100;
        static void Main(string[] args)
        {
            //List<Visitante> visitantes = new List<Visitante>();
            //Extranjero oExtranjero = new Extranjero(20, 5000, 13, 100);
            //visitantes.Add(oExtranjero);
            //Visitante oMenor = new Nacional(15, 500, 13);
            //visitantes.Add(oMenor);
            //Visitante oMayor = new Nacional(33, 1000, 13);
            //visitantes.Add(oMayor);
            //foreach (Visitante aux in visitantes)
            //{
            //    Console.WriteLine(aux.ToString());
            //    Console.WriteLine("*********************************************************");

            //}

            //Console.ReadLine();

            bool again = true;
            List<Visitante> visitantesNacionales = new List<Visitante>();
            List<Visitante> visitantesExtranjeros = new List<Visitante>();

            do
            {
                Console.Clear();
                Console.WriteLine("Parque de Diversiones\n" +
                    "********************Menu********************\n");
                Console.Write("Tipo de Visitante \n" + "Registrar Entrada\n" +
                    "1 -> Nacional\n" +
                    "2 -> Extranjero\n" +
                    "3 -> Ver Nacionales\n" +
                    "4 -> Ver Extrajeros\n" +
                    "5 -> Borrar Nacionales\n" +
                    "6 -> Borrar Extrajeros\n" +
                    "7 -> Salir\n");

                int op = ReadInt("Digite una opción: "); ;
                int edad;
                switch (op)
                {
                    case 1:
                        Console.WriteLine("Nacional");
                        edad = ReadInt("Digite la edad");
                        if (edad >= 18)
                            visitantesNacionales.Add(new Nacional(edad, costoMayores, porcentajeImpuesto));
                        else
                            visitantesNacionales.Add(new Nacional(edad, costoMenores, porcentajeImpuesto));
                        break;
                    case 2:
                        Console.WriteLine("Extranjero");
                        edad = ReadInt("Digite la edad");
                        if (edad >= 18)
                            visitantesExtranjeros.Add(new Extranjero(edad, costoMayores, porcentajeImpuesto, tarifaImpuestoExtranjeros));
                        else
                            visitantesExtranjeros.Add(new Extranjero(edad, costoMenores, porcentajeImpuesto, tarifaImpuestoExtranjeros));
                        break;
                    case 3:
                        if (visitantesNacionales.Count == 0)
                        {
                            Console.WriteLine("Sin registros");
                        }
                        else
                        {
                            Console.WriteLine("Nacionales Registrados");
                            foreach (Visitante aux in visitantesNacionales)
                            {
                                Console.WriteLine(aux.ToString());
                                Console.WriteLine("*********************************************************");
                            }
                        }
                        break;
                    case 4:
                        if (visitantesExtranjeros.Count == 0)
                        {
                            Console.WriteLine("Sin registros");
                        }
                        else
                        {
                            Console.WriteLine("Extranjeros Registrados");
                            foreach (Visitante aux in visitantesExtranjeros)
                            {
                                Console.WriteLine(aux.ToString());
                                Console.WriteLine("*********************************************************");
                            }
                        }
                        break;
                    case 5:
                        VaciarLista(visitantesNacionales);
                        break;
                    case 6:
                        VaciarLista(visitantesExtranjeros);
                        break;


                }

                Console.WriteLine("Digite para " + (op == 7 ? "Salir" : "Continuar"));
                again = (op == 7 ? false : true);
                Console.ReadLine();
            }
            while (again);
        }

        private static void VaciarLista(List<Visitante> lista)
        {
            bool eliminar;
            Console.WriteLine("Desea eliminar la lista? (si = y/no = n)");
            string data = Console.ReadLine();
            eliminar = (data == "y" ? true : false);
            if (eliminar)
            {
                lista.Clear();
            }
            else
            {
                Console.WriteLine("Borrar cancelado");
            }
        }

        /// <summary>
        /// Captura un valor de tipo Double por consola
        /// </summary>
        /// <param name="text">Texto para mostrar al usuario</param>
        /// <returns>valor double capturado</returns>
        internal static double ReadDouble(string text = "Digite el valor")
        {
            bool result = false;
            START:
            Console.WriteLine(text);

            string data = Console.ReadLine();
            result = Double.TryParse(data, out double value);
            if (!result)
            {
                Console.WriteLine("Intente de nuevo");
                goto START;
            }
            return value;
        }
        /// <summary>
        /// Captura un valor de tipo Int por consola
        /// </summary>
        /// <param name="text">Texto para mostrar al usuario</param>
        /// <returns>Valor entero capturado</returns>
        internal static int ReadInt(string text = "Digite un numero entero")
        {
            bool result = false;
            START:
            Console.WriteLine(text);

            string data = Console.ReadLine();
            result = Int32.TryParse(data, out int value);
            if (!result)
            {
                Console.WriteLine("Intente de nuevo");
                goto START;
            }
            return value;
        }

    }
}
