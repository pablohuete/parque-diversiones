﻿using ParqueDiversiones.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueDiversiones.Clases
{
    internal class Extranjero : Visitante
    {
        public double TarifaImpuestoExtranjeros { get; set; }
        public Extranjero(int edad, double costo, double porcentajeImpuesto, double tarifaImpuestoExtranjeros) :
            base(edad, costo, porcentajeImpuesto)
        {
            this.Origen = Nacionalidad.Extrangero;
            this.TarifaImpuestoExtranjeros = TarifaImpuestoExtranjeros;
        }
        public override double CalcularImpuesto()
        {
            return (this.Costo * this.PorcentajeImpuesto) / 100f + this.TarifaImpuestoExtranjeros;
        }

        public override double ObtenerTotal()
        {
            return this.Costo + this.CalcularImpuesto();
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append("Nacionalidad: " + this.Origen.ToString() + " ");
            str.Append("Edad: " + this.Edad.ToString() + " ");
            str.Append("Total a pagar: " + this.ObtenerTotal().ToString() + " ");

            return str.ToString();
        }
    }
}
