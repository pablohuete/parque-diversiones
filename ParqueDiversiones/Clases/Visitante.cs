﻿using ParqueDiversiones.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueDiversiones.Clases
{
    abstract class Visitante
    {
        public int Edad { get; set; }
        public Nacionalidad Origen { get; set; }
        public double Costo { get; set; }
        public double PorcentajeImpuesto { get; set; }

        public Visitante(int edad, double costo, double porcentajeImpuesto)
        {
            this.Edad = edad;
            this.Costo = costo;
            this.PorcentajeImpuesto = porcentajeImpuesto;
        }

        public abstract double ObtenerTotal();
        public abstract double CalcularImpuesto();
        public abstract string ToString();
    }
}
